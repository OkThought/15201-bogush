//
//  tuple_parser.h
//  bogush
//
//  Created by Иван Богуш on 12/25/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef tuple_parser_h
#define tuple_parser_h

#include <iostream>
#include <sstream>
#include <tuple>
#include <vector>

namespace {
	using namespace std;

	template <class T>
	tuple<T> wrapInTuple(const string& str) {
		T value;
		stringstream ss(str);
		ss >> value;
		if (ss.fail()) {
			throw invalid_argument("couldn't parse format string: " + str);
		}
		return tuple<T>(value);
	}

	template <>
	tuple<string> wrapInTuple(const string& str) {
		return tuple<string>(str);
	}

	template <class ...Types> struct TupleParser;

	template <class T>
	struct TupleParser<T> {
		static tuple<T>
		parse(size_t index, const vector<string>& items) {
			string item = items[index];
			return wrapInTuple<T>(item);
		}
	};

	template <class T, class ...Types>
	struct TupleParser<T, Types...> {
		static tuple<T, Types...>
		parse(size_t index, const vector<string>& items) {
			string item = items[index];
			return tuple_cat(wrapInTuple<T>(item),
							 TupleParser<Types...>::parse(index+1, items));
		}
	};
} // anonymous namespace
#endif /* tuple_parser_h */
