//
//  tuple_printer.h
//  bogush
//
//  Created by Иван Богуш on 12/19/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef tupleprinter_h
#define tupleprinter_h

#include <iostream>

template <size_t I, size_t Size, class ...Args>
struct TuplePrinter {
	static void print (std::ostream& os, std::tuple<Args...> const& t) {
		os << std::get<I>(t) << " ";
		TuplePrinter<I+1, Size, Args...>::print(os, t);
	}
};

template <size_t I, class ...Args>
struct TuplePrinter<I, I, Args...> {
	static void print (std::ostream&, std::tuple<Args...> const&) {}
};

template <class Ch, class Tr, class ...Args>
std::basic_ostream<Ch, Tr>&
operator<< (std::basic_ostream<Ch, Tr>& os,
			std::tuple<Args...> const& t) {
	TuplePrinter<0, sizeof...(Args), Args...>::print(os, t);
	return os;
}

#endif /* tupleprinter_h */
