//
//  csv_parser.h
//  bogush
//
//  Created by Иван Богуш on 12/23/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef csv_parser_h
#define csv_parser_h

#include "tuple_parser.h"

#include <iostream>
#include <tuple>
#include <sstream>

namespace {

	/*--------------------CSVParser--------------------*/

	template <class ...Types>
	class CSVParser {
	public:
		class Parser;

		static const char default_comma = ',';
		static const char default_row_separator = '\n';
		static const char default_escape = '"';
		static const size_t row_size = sizeof...(Types);

		CSVParser(istream& customIStream = cin,
				  char customComma = default_comma,
				  char customRowSep = default_row_separator,
				  char customEscape = default_escape):
		comma(customComma),
		rowSep(customRowSep),
		escape(customEscape),
		stream(customIStream) {
			if (customComma == customRowSep) {
				throw invalid_argument
				("comma shouldn't be equal to row separator");
			} else if (customEscape == customComma) {
				throw invalid_argument
				("comma shouldn't be equal to escape symbol");
			} else if (customEscape == customRowSep) {
				throw invalid_argument
				("escape symbol shouldn't be equal to row separator");
			}
		}

		using input_iterator = typename Parser::input_iterator;

		input_iterator begin() {
			auto it = input_iterator(*this, Parser(stream, comma, rowSep, escape), true);
			return ++it;
		}

		inline input_iterator end() {
			return input_iterator(*this, Parser(stream, comma, rowSep, escape), false);
		}
		const char comma;
		const char rowSep;
		const char escape;
		istream& stream;
	private:
		size_t rowCnt = 0;
		typename Parser::value_type lastValue;
	};

	/*--------------------Parser--------------------*/

	template <class ...Types>
	class CSVParser<Types...>::Parser {
	public:
		using value_type = tuple<Types...>;
		class input_iterator;

		Parser(istream& customIStream,
			   char customComma,
			   char customRowSep,
			   char customEscape):
		comma(customComma),
		rowSep(customRowSep),
		escape(customEscape),
		stream(customIStream) {

		}

	private:
		value_type parseNextRow() {
			vector<string> fields = parseFields();
			if (fields.empty()) {
				return value_type{};
			}
			return parseValue(fields);
		}

		vector<string> parseFields() {
			vector<string> fields(row_size);
			++rowCnt;
			colCnt = 0;
			chCnt = 0;
			field.erase();
			escaping = false;
			previous = comma;

			if (stream.get(current)) {
				parse(fields);
			} else {
				fields.clear();
				return fields;
			}

			if (escaping) {
				// still escaping: something went wrong
				throw runtime_error("end of file occured while escaping");
			} else {
				fields[colCnt] = field;
				return fields;
			}
		}

		value_type parseValue(const vector<string>& fields) {
			auto size = fields.size();
			if (size != row_size) {
				throw invalid_argument("wrong number of fields in the row: " +
									   to_string(size));
			}
			return TupleParser<Types...>::parse(0, fields);
		}

		void parse(vector<string>& fields) {
			do {
				if (current == escape) {
					if (parse_escape(fields)) continue;
				} else if (current == comma) {
					if (parse_comma(fields)) continue;
				} else if (current == rowSep) {
					if (!escaping) {
						// row is read
						fields[colCnt] = field;
						return;
					}
				}
				++chCnt;
				field += current;
				previous = current;
			} while (stream.get(current));
		}

		bool parse_escape(vector<string>& fields) {
			if (escaping) {
				if (stream.get(next)) {
					if (next == escape) {
						// embedded double-escape: skip one
						++chCnt;
						current = next;
						previous = current;
					} else {
						// stop escaping
						escaping = false;
						stream.unget();
						return true;
					}
				} else {
					// stream failed on reading next character after escape
					// stop escaping, row is read
					escaping = false;
					fields[colCnt] = field;
					return true;
				}
			} else {
				if (previous == comma) {
					// start escaping
					escaping = true;
					++chCnt;
					return true;
				} else {
					throw runtime_error
					("error while parsing at " +
					 currentPositionToString() +
					 ": each embedded escape character should be escaped");
				}
			}
			return false;
		}

		bool parse_comma(vector<string>& fields) {
			if (escaping) {
				return false;
			} else {
				fields[colCnt++] = field;
				field.erase();
				previous = comma;
				if (colCnt >= row_size) {
					throw runtime_error
					("error while parsing at " +
					 currentPositionToString() +
					 ": too many fields passed in the row");
				}
				return true;
			}
		}

		inline string currentPositionToString() const {
			return	"row " + to_string(rowCnt) +
					" column " + to_string(colCnt + 1) +
					" symbol " + to_string(chCnt + 1);
		}

		const char comma;
		const char rowSep;
		const char escape;
		istream& stream;
		string field;
		size_t rowCnt = 0;
		size_t colCnt = 0;
		size_t chCnt = 0;
		bool escaping;
		char previous;
		char current;
		char next;
	};

	/*--------------------input_iterator--------------------*/

	template <class ...Types>
	class CSVParser<Types...>::Parser::input_iterator {
	public:
		using reference = input_iterator&;
		using const_reference = const input_iterator&;

		input_iterator(CSVParser& csvParser,
					   Parser customParser,
					   bool isValid):
		csv(csvParser),
		parser(customParser),
		valid(isValid) {

		}

		input_iterator(const input_iterator& other):
		csv(other.csv),
		parser(other.parser),
		value(other.value),
		valid(other.valid),
		rowCnt(other.rowCnt)
		{

		}

		input_iterator(input_iterator&& other):
		csv(other.csv),
		parser(other.parser),
		value(other.value),
		valid(other.valid),
		rowCnt(other.rowCnt)
		{
			other.valid = false;
		}

		inline input_iterator& operator= (const input_iterator& other) {
			csv = other.csv;
			parser = other.parser;
			value = other.value;
			valid = other.valid;
			rowCnt = other.rowCnt;
			return *this;
		}

		inline input_iterator& operator= (input_iterator&& other) {
			csv = other.csv;
			parser = other.parser;
			value = other.value;
			valid = other.valid;
			rowCnt = other.rowCnt;
			other.valid = false;
			return *this;
		}

		inline value_type& operator* () {
			return value;
		}

		inline value_type& operator->() {
			return value;
		}

		input_iterator operator++() {
			if (valid) {
				if (is_actual()) {
					incrementRow();
					value = parser.parseNextRow();
					csv.lastValue = value;
				} else if (is_previous()) {
					++rowCnt;
					++parser.rowCnt;
					value = csv.lastValue;
				}
				valid = !parser.stream.eof();
				valid &= is_actual();
			}
			return *this;
		}

		inline input_iterator operator++(int) {
			auto copy = *this;
			++(*this);
			return copy;
		}

		bool operator==(const_reference other) const {
			if (!is_valid() && !other.is_valid()) return true;
			if (is_valid() != other.is_valid()) return false;
			return value == other.value;
		}

		inline bool operator!=(const_reference other) const {
			return !(*this == other);
		}

	private:
		inline bool is_previous() const {
			return rowCnt + 1 == csv.rowCnt;
		}

		inline bool is_actual() const {
			return rowCnt == csv.rowCnt;
		}

		inline bool is_valid() const {
			return valid && is_actual();
		}

		inline void incrementRow() {
			++rowCnt;
			++csv.rowCnt;
		}

		CSVParser& csv;
		Parser parser;
		value_type value;
		bool valid;
		size_t rowCnt = 0;
	};
} // anonymous namespace


#endif /* csv_parser_h */
