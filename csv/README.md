CSV-Parser
==========
The third c++ task I finished.

What is it about?
-----------------
* It implements a simple algorithm that reads continuous [comma-separated-values](https://en.wikipedia.org/wiki/Comma-separated_values) files.
* Parses the text into fields
* And fills cpecified tuples with them.
* So that you can easily iterate between parsed table-rows represented as tuples.

Summary
-------
* You can parse any `stream` object.
* `CSVParser` can escape sequences, i.e. `"He said ""Hello!"""` would be escaped into `He said "Hello!"`
* You can specify in constructor which characters to use as commas, line-breaks and escape symbols.
* There is also a `class TuplePrinter` which adds recursive templates to print out data from tuple. (i.e. `cout << make_tuple(1,2.0,"Hello World");`)

Example of usage
----------------
1. To parse a simple file `input.csv` into table with three columns with value types as follows: int, float, string. You can use this code:

        ```c++
        std::ifstream input("input.csv");
        CSVParser<int, float, std::string> parser(input);
        typedef std::tuple<int, float, std::string> Row;
        // Row is a tuple corresponding to our expectations about table structure
        std::vector<Row> table(parser.begin(), parser.end());
        // table is a vector of Rows
        ```

2. More complex example

        ```c++
        using namespace std;
        stringstream stream(
            "1:'''Harry Potter''';"
            "2:' ''Sherlock Holmes'' book series'"
        );
        char comma = ':';
        char rowSeparator = ';';
        char escapeSymbol = '\'';
        CSVParser<int, string> parser(stream, comma, rowSeparator, escapeSymbol);
        for (const auto& eachRow: parser) {
            cout << eachRow << endl;
        }
        ```
