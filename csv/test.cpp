//
//  test.cpp
//  bogush
//
//  Created by Иван Богуш on 1/19/17.
//  Copyright © 2017 Ivan Bogush. All rights reserved.
//

#include "gtest/gtest.h"
#include "tuple_printer.h"
#include "csv_parser.h"

#include <sstream>
#include <tuple>

using namespace std;

TEST(Parser, empty_stream) {
	stringstream stream{""};
	CSVParser<int> parser{stream};
	EXPECT_EQ(parser.end(), parser.begin());
}

TEST(Parser, simple_input) {
	stringstream stream{"1984"};
	CSVParser<int> parser{stream};
	tuple<int> value(1984);
	auto it = parser.begin();
	EXPECT_EQ(value, *it);
	EXPECT_EQ(parser.end(), ++it);
}

TEST(Parser, escape_sequence) {
	stringstream stream{"\"some string\""};
	CSVParser<string> parser{stream};
	tuple<string> value("some string");
	auto it = parser.begin();
	EXPECT_EQ(value, *it);
	EXPECT_EQ(parser.end(), ++it);
}

TEST(Parser, escape_row_separator) {
	stringstream stream{"\"\n\""};
	CSVParser<string> parser{stream};
	tuple<string> value("\n");
	auto it = parser.begin();
	EXPECT_EQ(value, *it);
	EXPECT_EQ(parser.end(), ++it);
}

TEST(Parser, escape_comma) {
	stringstream stream{"\"1,1\",\",2,\",3"};
	CSVParser<string, string, string> parser{stream};
	tuple<string, string, string> value("1,1", ",2,", "3");
	auto it = parser.begin();
	EXPECT_EQ(value, *it);
	EXPECT_EQ(parser.end(), ++it);
}

TEST(Parser, escape_escape) {
	stringstream stream{"\"\"\"\""};
	CSVParser<string> parser{stream};
	tuple<string> value("\"");
	auto it = parser.begin();
	EXPECT_EQ(value, *it);
	EXPECT_EQ(parser.end(), ++it);
}

TEST(Parser, escape_escapes) {
	stringstream stream{"\"\"\"In the beginning\"\"\""};
	CSVParser<string> parser{stream};
	tuple<string> value("\"In the beginning\"");
	auto it = parser.begin();
	EXPECT_EQ(value, *it);
	EXPECT_EQ(parser.end(), ++it);
}

TEST(Parser, multiple_rows) {
	stringstream stream{"1,1,2\n3,5,8\n13,21,34"};
	CSVParser<int, int, int> parser{stream};
	vector<tuple<int, int, int>> values{
		make_tuple(1,1,2),
		make_tuple(3,5,8),
		make_tuple(13,21,34)
	};
	auto it = parser.begin();
	for (const auto& value: values) {
		EXPECT_EQ(value, *it);
		++it;
	}
	EXPECT_EQ(parser.end(), it);
}


TEST(Parser, custom_service_characters) {
	stringstream stream{"'''Excellent!''' I cried.'''Elementary''', said he."};
	CSVParser<string, string, string> parser{stream, ' ', '.', '\''};
	vector<tuple<string, string, string>> values{
		make_tuple("'Excellent!'","I","cried"),
		make_tuple("'Elementary',","said","he")
	};
	auto it = parser.begin();
	for (const auto& value: values) {
		EXPECT_EQ(value, *it);
		++it;
	}
	EXPECT_EQ(parser.end(), it);
}

TEST(Parser, too_many_fields) {
	stringstream stream{"1,1,2\n3,5,8\n13,21,34"};
	CSVParser<int, int> parser{stream};
	EXPECT_ANY_THROW(parser.begin());
}

TEST(Parser, too_few_fields) {
	stringstream stream{"1,1,2\n3,5,8\n13,21,34"};
	CSVParser<int, int, int, int> parser{stream};
	EXPECT_ANY_THROW(parser.begin());
}

TEST(Parser, not_escaped_embedded_escape) {
	stringstream stream{"Sherlock\"Holmes"};
	CSVParser<string> parser{stream};
	EXPECT_ANY_THROW(parser.begin());
}

TEST(Parser, escaped_single_embedded_escape) {
	stringstream stream{"\"Dr.\"Watson\""};
	CSVParser<string> parser{stream};
	EXPECT_ANY_THROW(parser.begin());
}

TEST(Parser, spaces_are_not_trimmed) {
	stringstream stream{"   \"Mrs. Hudson\"   "};
	CSVParser<string> parser{stream};
	EXPECT_ANY_THROW(parser.begin());
}

TEST(Parser, bad_custom_service_characters) {
	// they shouldn't repeat each other
	EXPECT_ANY_THROW((CSVParser<int>{cin, 'f', 'f', 'u'}));
	EXPECT_ANY_THROW((CSVParser<int>{cin, 'a', 'a', 'a'}));
	EXPECT_ANY_THROW((CSVParser<int>{cin, 'l', 'o', 'l'}));
	EXPECT_ANY_THROW((CSVParser<int>{cin, 'f', 'o', 'o'}));
}

TEST(Parser, two_singlepass_iterators) {
	stringstream stream{"'''Excellent!''' I cried.'''Elementary''', said he."};
	CSVParser<string, string, string> parser{stream, ' ', '.', '\''};
	auto it1 = parser.begin();
	auto it2 = parser.begin();
	EXPECT_EQ(*it1, *it2);
	EXPECT_EQ(it1, it2);
}

TEST(Parser, two_iterators) {
	stringstream stream{"'''Excellent!''' I cried.'''Elementary''', said he."};
	CSVParser<string, string, string> parser{stream, ' ', '.', '\''};
	auto it1 = parser.begin();
	auto it2 = it1;
	ASSERT_EQ(it1, it2);
	vector<tuple<string, string, string>> values{
		make_tuple("'Excellent!'","I","cried"),
		make_tuple("'Elementary',","said","he")
	};
	for (const auto& value: values) {
		ASSERT_EQ(value, *it2);
		++it2;
	}
	EXPECT_EQ(parser.end(), it2);
}
