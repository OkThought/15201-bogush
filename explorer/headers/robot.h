//
//  robot.h
//  explorer
//
//  Created by Иван Богуш on 11/22/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef robot_h
#define robot_h

#include "surface.h"

#include <vector>

template <typename S>
class Robot {
public:
	using surface_type = S;
	using point_type = typename surface_type::point_type;
	using metric_type = typename surface_type::metric_type;
	using point_sequence = typename surface_type::point_sequence;
	using point_set = typename surface_type::point_set;

	Robot(const surface_type& sur):
	surface(sur) {
		if(sur.empty()) {
			throw std::runtime_error("empty surface");
		}
	}

	point_sequence
	findPath(const point_type& from, const point_type& to)
	throw(std::runtime_error) {
		auto path = searchPath(from, to);
		if (surface.checkPath(path, from, to)) {
			return path;
		}
		throw std::runtime_error("path not found or incorrect");
	}

	virtual point_sequence
	searchPath(const point_type& from, const point_type& to)
		throw(std::runtime_error) = 0;

protected:
	const surface_type& surface;
};

#endif /* robot_h */
