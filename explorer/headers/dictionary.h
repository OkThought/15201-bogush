//
//  dictionary.h
//  bogush
//
//  Created by Иван Богуш on 12/9/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef dictionary_h
#define dictionary_h

#include "surface.h"

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>

class Dictionary: public Surface<std::string, size_t, std::vector<std::string>, std::unordered_set<std::string>> {
public:
	using data_type = std::unordered_map<std::string, point_set>;
	using field_type = point_sequence;
	Dictionary(): data() {}
	Dictionary(const field_type& field);
	~Dictionary() {};

	Dictionary& setData(const field_type& field);
	data_type getData() const;
	field_type getField() const;
	metric_type distance(const point_type& from, const point_type& to) const final;
	point_set lookup(const point_type& at) const final;
	bool checkPath(const point_sequence& path, const point_type& from, const point_type& to) const final;
	bool empty() const final;
private:
	data_type data;
	point_sequence templates(const point_type& word) const;
};

#endif /* dictionary_h */
