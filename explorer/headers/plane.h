//
//  plane.h
//  explorer
//
//  Created by Иван Богуш on 11/22/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef plane_h
#define plane_h

#include "planar.h"

class Plane : public Planar {
public:
	Plane();
	Plane(const field_type& field);
	~Plane() {};

private:
	metric_type horizontalDistance(const point_type& from, const point_type& to) const final;
	metric_type verticalDistance(const point_type& from, const point_type& to) const final;
	point_type westernNeighbor(const point_value_type x, const point_value_type y) const final;
	point_type easternNeighbor(const point_value_type x, const point_value_type y) const final;
	point_type northernNeighbor(const point_value_type x, const point_value_type y) const final;
	point_type southernNeighbor(const point_value_type x, const point_value_type y) const final;
};

#endif /* plane_h */
