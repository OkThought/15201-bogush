//
//  torus.h
//  bogush
//
//  Created by Иван Богуш on 12/2/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef torus_h
#define torus_h

#include "planar.h"

class Torus : public Planar {
public:
	Torus() = default;
	Torus(const field_type& field);
	~Torus() {};

private:
	metric_type horizontalDistance(const point_type& from, const point_type& to) const final;
	metric_type verticalDistance(const point_type& from, const point_type& to) const final;
	point_type westernNeighbor(const point_value_type x, const point_value_type y) const final;
	point_type easternNeighbor(const point_value_type x, const point_value_type y) const final;
	point_type northernNeighbor(const point_value_type x, const point_value_type y) const final;
	point_type southernNeighbor(const point_value_type x, const point_value_type y) const final;

	point_type modulus(point_value_type x, point_value_type y) const;
	point_type modulus(const point_type& point) const;
};
#endif /* torus_h */
