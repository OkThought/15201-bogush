//
//  dictionary_serializer.h
//  bogush
//
//  Created by Иван Богуш on 12/12/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef dictionary_serializer_h
#define dictionary_serializer_h

#include "serializer.h"
#include "dictionary.h"

#include <iostream>

class DictionarySerializer: public Serializer<Dictionary> {
public:
	using word_type = std::string;
	using word_set = std::vector<word_type>;

	static word_set split(const word_type& str, const word_type& delimiter);
	void read(std::istream& is, Dictionary& dict) final;
	void read(std::istream& is, Dictionary& dict, const word_type& delimiter);
	void print(std::ostream& os, const Dictionary& dict) final;
	void printPath(std::ostream& os, const Dictionary& dict, const point_sequence& path) final;
};


#endif /* dictionary_serializer_h */
