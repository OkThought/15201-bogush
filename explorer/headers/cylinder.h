//
//  cylinder.h
//  bogush
//
//  Created by Иван Богуш on 12/2/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef cylinder_h
#define cylinder_h

#include "planar.h"

class Cylinder : public Planar {
public:
	Cylinder();
	Cylinder(const field_type& field);
	~Cylinder() {};

private:
	metric_type horizontalDistance(const point_type& from, const point_type& to) const final;
	metric_type verticalDistance(const point_type& from, const point_type& to) const final;
	point_type westernNeighbor(point_value_type x, point_value_type y) const final;
	point_type easternNeighbor(point_value_type x, point_value_type y) const final;
	point_type northernNeighbor(point_value_type x, point_value_type y) const final;
	point_type southernNeighbor(point_value_type x, point_value_type y) const final;

	point_type modulus(point_value_type x, point_value_type y) const;
	point_type modulus(const point_type& point) const;
};

#endif /* cylinder_h */
