//
//  serializer.h
//  bogush
//
//  Created by Иван Богуш on 12/7/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef serializer_h
#define serializer_h

#include "surface.h"

#include <iostream>

template <class S>
struct Serializer {
	using point_sequence = typename S::point_sequence;
	virtual void read(std::istream& is, S&) = 0;
	virtual void print(std::ostream& os, const S&) = 0;
	virtual void printPath(std::ostream& os, const S&,
						   const point_sequence& path) = 0;
};

#endif /* serializer_h */
