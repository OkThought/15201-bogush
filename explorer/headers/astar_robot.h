//
//  astar_robot.h
//  bogush
//
//  Created by Иван Богуш on 11/28/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef astar_robot_h
#define astar_robot_h

#include "planar.h"
#include "robot.h"

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <functional> // function
#include <algorithm> // reverse

template <class S>
struct NonHeuristic {
	using point_type = typename S::point_type;
	using metric_type = typename S::metric_type;
	metric_type operator() (point_type, point_type) { return 0; }
};

template <class S, class H = NonHeuristic<S>>
class AStarRobot: public Robot<S> {
public:
	using point_type = typename Robot<S>::point_type;
	using metric_type = typename Robot<S>::metric_type;
	using surface_type = typename Robot<S>::surface_type;
	using point_sequence = typename Robot<S>::point_sequence;
	using point_set = typename Robot<S>::point_set;
	using Robot<S>::surface;
	struct LessCost {
		LessCost(const AStarRobot& r): robot(r) {

		}

		bool operator () (const point_type& left,
						  const point_type& right) const {
			if (robot.cost. count(right) == 0) {
				return true; // smth < infinity
			}
			if (robot.cost.count(left) == 0) {
				return false; // infinity > smth
			}
			return robot.cost.at(left) < robot.cost.at(right);
		}

		const AStarRobot& robot;
	};

	AStarRobot(const surface_type& s):
	Robot<S>(s),
	open(LessCost(*this)) {

	}

	virtual ~AStarRobot() {

	}

	point_sequence
	searchPath(const point_type& from, const point_type& to)
		throw (std::runtime_error) override;

private:
	point_sequence reconstruct(const point_type& p) const;
	point_type extractClosestOpen();

	std::unordered_map<point_type, point_type> cameFrom;
	std::unordered_map<point_type, metric_type> cost;
	std::unordered_map<point_type, metric_type> total;
	std::set<point_type, LessCost> open;
	std::unordered_set<point_type> closed;
	std::function<metric_type(point_type, point_type)> heuristic = H();
};

template <class S, class H>
typename AStarRobot<S, H>::point_sequence
AStarRobot<S, H>::reconstruct(const point_type& p) const {
	auto current = p;
	point_sequence path = { current };
	while (cameFrom.count(current) != 0) {
		current = cameFrom.at(current);
		path.push_back(current);
	}
	std::reverse(path.begin(), path.end());
	return path;
}

template <class S, class H>
typename AStarRobot<S, H>::point_type
AStarRobot<S, H>::extractClosestOpen() {
	auto it_closest = open.begin();
	open.erase(it_closest);
	return *it_closest;
}

template <class S, class H>
typename AStarRobot<S, H>::point_sequence
AStarRobot<S, H>::searchPath(const point_type& from, const point_type& to)
throw(std::runtime_error) {
	cost = { { from, 0 } };
	total = { { from, heuristic(from, to) } };
	open = { from };

	while (!open.empty()) {
		point_type current = extractClosestOpen();
		if (current == to) {
			return reconstruct(current);
		}
		closed.insert(current);
		point_set neighbors = surface.lookup(current);
		for (auto neighbor: neighbors) {

			if (closed.count(neighbor) != 0) {
				// ignore visited
				continue;
			}

			auto dist = surface.distance(current, neighbor);
			auto tentative = cost[current] + dist;

			if (open.count(neighbor) == 0) {
				// discover a new node
				open.insert(neighbor);
			} else if (tentative >= cost[neighbor]) {
				// this path is not better
				continue;
			}

			// this path is the best until now
			cameFrom[neighbor] = current;
			cost[neighbor] = tentative;
			total[neighbor] = tentative + heuristic(neighbor, to);
		}
	}
	throw std::runtime_error("couldn't find path");
}

#endif /* astar_robot_h */
