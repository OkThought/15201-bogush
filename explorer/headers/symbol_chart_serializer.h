//
//  symbol_chart_serializer.h
//  bogush
//
//  Created by Иван Богуш on 12/7/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef symbol_chart_serializer_h
#define symbol_chart_serializer_h

#include "serializer.h"
#include "planar.h"

#include <iostream>

struct SymbolChartSerializer: public Serializer<Planar> {
	char Start	= 'S';
	char Finish	= 'F';
	char Wall	= '#';
	char Space	= '.';
	char Path	= '*';

	Planar::point_type start;
	Planar::point_type finish;

	SymbolChartSerializer() {}

	void read(std::istream& is, Planar& planar) final;
	void print(std::ostream& os, const Planar& planar) final;
	void printPath(std::ostream& os, const Planar& planar, const Planar::point_sequence& path) final;
};

#endif /* symbol_chart_serializer_h */
