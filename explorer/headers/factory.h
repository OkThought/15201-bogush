//
//  factory.h
//  bogush
//
//  Created by Иван Богуш on 12/7/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef factory_h
#define factory_h

#include "planar.h"

#include <memory> // shared_ptr
#include <string>
#include <unordered_map>

class AbstractPlanarCreator {
public:
	AbstractPlanarCreator() {};
	virtual std::shared_ptr<Planar> create() const = 0;
};

template <class C>
class PlanarCreator: public AbstractPlanarCreator {
public:
	std::shared_ptr<Planar> create() const final {
		return std::shared_ptr<Planar> (new C());
	}
};

class PlanarFactory {
public:
	using id_type = std::string;
	using factory_map = std::unordered_map<id_type, std::shared_ptr<AbstractPlanarCreator>>;

	template <class C>
	PlanarFactory& add(const id_type& id) {
		auto it = factory.find(id);
		if (it == factory.end()) {
			factory[id] = std::shared_ptr<PlanarCreator<C>>(new PlanarCreator<C>());
		}
		return *this;
	}

	std::shared_ptr<Planar> create(const id_type& id) {
		auto it = factory.find(id);
		if (it != factory.end()) {
			return it->second->create();
		}
		return nullptr;
	}
private:
	factory_map factory;
};

#endif /* factory_h */
