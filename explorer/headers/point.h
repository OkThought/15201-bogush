//
//  point.h
//  explorer
//
//  Created by Иван Богуш on 11/22/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef point_h
#define point_h

#include <string>
#include <cmath>

template <typename V>
struct Point {
public:
	using value_type = V;
	value_type x;
	value_type y;

	Point();
	Point(value_type same);
	Point(value_type xx, value_type yy);

	value_type square() const;
	value_type magnitude() const;

	bool operator== (const Point& other) const;
	bool operator!= (const Point& other) const;

	operator std::string() const;

	Point& operator+= (const Point& other);
	Point& operator-= (const Point& other);
};

template <typename V>
Point<V>::Point():
x(0), y(0) {

}

template <typename V>
Point<V>::Point(Point::value_type same):
x (same), y(same) {

}

template <typename V>
Point<V>::Point(Point::value_type xx, Point::value_type yy):
x (xx), y(yy) {

}

template <typename V>
typename Point<V>::value_type Point<V>::square() const {
	return x*x + y*y;
}

template <typename V>
typename Point<V>::value_type Point<V>::magnitude() const {
	return sqrt(square());
}

template <typename V>
bool Point<V>::operator==(const Point &other) const {
	return x == other.x && y == other.y;
}

template <typename V>
bool Point<V>::operator!=(const Point &other) const {
	return !(*this == other);
}

template <typename V>
Point<V>::operator std::string() const {
	return "(" + std::to_string(x) + ", " + std::to_string(y) + ")";
}

template <typename V>
Point<V>& Point<V>::operator+=(const Point &other) {
	x += other.x;
	y += other.y;
	return *this;
}

template <typename V>
Point<V>& Point<V>::operator-=(const Point &other) {
	x -= other.x;
	y -= other.y;
	return *this;
}

template <typename V>
Point<V> operator+ (const Point<V>& left, const Point<V>& right) {
	Point<V> result = left;
	result += right;
	return result;
}

template <typename V>
Point<V> operator- (const Point<V>& left, const Point<V>& right) {
	Point<V> result = left;
	result -= right;
	return result;
}

template <typename V>
std::ostream& operator << (std::ostream& os, const Point<V> &p) {
	os << (std::string) p;
	return os;
}

template <>
Point<size_t> operator- (const Point<size_t>& left, const Point<size_t>& right) = delete;

template <>
Point<size_t>& Point<size_t>::operator-= (const Point& other) = delete;

#endif /* point_h */
