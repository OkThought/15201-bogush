//
//  surface.h
//  explorer
//
//  Created by Иван Богуш on 11/22/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef surface_h
#define surface_h

#include <vector>
#include <cstddef>

// template-based Surface interface
template <typename P, typename M = size_t, typename V = std::vector<P>, typename S = V>
class Surface {
public:
	using point_type = P;
	using metric_type = M;
	using point_sequence = V;
	using point_set = S;

	virtual ~Surface() {};

	virtual metric_type distance(const point_type& from, const point_type& to) const = 0;
	virtual point_set lookup(const point_type& at) const = 0;
	virtual bool checkPath(const point_sequence& path, const point_type& from, const point_type& to) const = 0;
	virtual bool empty() const = 0;
};

#endif /* surface_h */
