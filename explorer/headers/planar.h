//
//  planar.h
//  bogush
//
//  Created by Иван Богуш on 12/7/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef planar_h
#define planar_h

#include "surface.h"
#include "point.h"

#include <iostream>

class Planar : public Surface<Point<size_t>, size_t> {
public:
	using field_type = std::vector<std::vector<bool>>;
	using point_value_type = point_type::value_type;
	enum Symbol {SPACE = false, WALL = true};
	using symbol_type = Symbol;

	Planar();
	Planar(const field_type& field);
	virtual ~Planar() {};

	metric_type distance(const point_type& from, const point_type& to) const final;
	point_set lookup(const point_type& at) const final;
	bool checkPath(const point_sequence& path, const point_type& from, const point_type& to) const final;
	bool empty() const final;

	symbol_type getSymbol(const point_type& at) const;
	symbol_type getSymbol(const point_value_type x, const point_value_type y) const;
	field_type getField() const;
	metric_type getHeight() const;
	metric_type getWidth() const;

	Planar& setSymbol(point_value_type x, point_value_type y, symbol_type s);
	Planar& setSymbol(const point_type& p, symbol_type s);
	Planar& setField(const field_type& f);


protected:
	bool exists(point_value_type x, point_value_type y) const;
	bool exists(const point_type& there) const;
	bool free(point_value_type x, point_value_type y) const;
	bool free(const point_type& there) const;

	virtual metric_type horizontalDistance(const point_type& from, const point_type& to) const = 0;
	virtual metric_type verticalDistance(const point_type& from, const point_type& to) const = 0;
	virtual point_type westernNeighbor(point_value_type x, point_value_type y) const;
	virtual point_type easternNeighbor(point_value_type x, point_value_type y) const;
	virtual point_type northernNeighbor(point_value_type x, point_value_type y) const;
	virtual point_type southernNeighbor(point_value_type x, point_value_type y) const;
	point_type westernNeighbor(const point_type& point) const;
	point_type easternNeighbor(const point_type& point) const;
	point_type northernNeighbor(const point_type& point) const;
	point_type southernNeighbor(const point_type& point) const;
	point_set neighborsOf(const point_type& point) const;

	metric_type width;
	metric_type height;
	field_type field;
};

namespace std {
	using point_type = Planar::point_type;
	using point_value_type = Planar::point_value_type;
	template<>
	struct hash<point_type> {
		size_t operator() (const point_type& p) const {
			return hash<point_value_type>()( p.x ) ^
			(hash<point_value_type>()( p.y ) << 1) >> 1;
		}
	};
}

#endif /* planar_h */
