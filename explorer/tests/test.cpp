//
//  test.cpp
//  bogush
//
//  Created by Иван Богуш on 12/8/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

//#include <string>
#include "surface.h"
#include "point.h"
#include "planar.h"
#include "plane.h"
#include "cylinder.h"
#include "torus.h"
#include "dictionary.h"
#include "serializer.h"
#include "symbol_chart_serializer.h"
#include "dictionary_serializer.h"
#include "factory.h"
#include "astar_robot.h"

#include <iostream>
#include <gtest/gtest.h>

using PlanarRobot = AStarRobot<Planar::Surface>;
using LexicalRobot = AStarRobot<Dictionary::Surface>;

TEST(symbol_chart_serializer, constructor) {
	SymbolChartSerializer serializer;
	EXPECT_EQ(serializer.start, Planar::point_type());
	EXPECT_EQ(serializer.finish, Planar::point_type());
}


TEST(symbol_chart_serializer, read_empty) {
	SymbolChartSerializer serializer;
	std::stringstream stream("");
	Plane plane;
	EXPECT_ANY_THROW(serializer.read(stream, plane));
	EXPECT_EQ(plane.getWidth(), 0);
	EXPECT_EQ(plane.getHeight(), 0);
	EXPECT_TRUE(plane.empty());
}

TEST(symbol_chart_serializer, read_empty_line) {
	SymbolChartSerializer serializer;
	std::stringstream stream("\n");
	Plane plane;
	EXPECT_ANY_THROW(serializer.read(stream, plane));
	EXPECT_EQ(plane.getWidth(), 0);
	EXPECT_EQ(plane.getHeight(), 0);
	EXPECT_TRUE(plane.empty());
}

TEST(symbol_chart_serializer, read_non_rectangular) {
	SymbolChartSerializer serializer;
	std::stringstream stream(".....\n"
							 ".......\n"
							 "...\n");
	Plane plane;
	EXPECT_ANY_THROW(serializer.read(stream, plane));
	EXPECT_EQ(plane.getWidth(), 0);
	EXPECT_EQ(plane.getHeight(), 0);
	EXPECT_TRUE(plane.empty());
}

TEST(symbol_chart_serializer, read_bad_char) {
	SymbolChartSerializer serializer;
	std::stringstream stream("...some waste...\n");
	Plane plane;
	EXPECT_ANY_THROW(serializer.read(stream, plane));
	EXPECT_EQ(plane.getWidth(), 0);
	EXPECT_EQ(plane.getHeight(), 0);
	EXPECT_TRUE(plane.empty());
}

TEST(symbol_chart_serializer, read_many_starts) {
	SymbolChartSerializer serializer;
	std::stringstream stream(".S.S.S\n");
	Plane plane;
	EXPECT_ANY_THROW(serializer.read(stream, plane));
	EXPECT_EQ(plane.getWidth(), 0);
	EXPECT_EQ(plane.getHeight(), 0);
	EXPECT_TRUE(plane.empty());
}

TEST(symbol_chart_serializer, read_many_finishes) {
	SymbolChartSerializer serializer;
	std::stringstream stream(".F.F.F\n");
	Plane plane;
	EXPECT_ANY_THROW(serializer.read(stream, plane));
	EXPECT_EQ(plane.getWidth(), 0);
	EXPECT_EQ(plane.getHeight(), 0);
	EXPECT_TRUE(plane.empty());
}

TEST(symbol_chart_serializer, read_one_point) {
	SymbolChartSerializer serializer;
	std::stringstream stream(".");
	Plane plane;
	serializer.read(stream, plane);
	EXPECT_EQ(plane.getWidth(), 1);
	EXPECT_EQ(plane.getHeight(), 1);
	EXPECT_FALSE(plane.empty());
}

TEST(symbol_chart_serializer, read_one_point_newline) {
	SymbolChartSerializer serializer;
	std::stringstream stream(".\n");
	Plane plane;
	serializer.read(stream, plane);
	EXPECT_EQ(plane.getWidth(), 1);
	EXPECT_EQ(plane.getHeight(), 1);
	EXPECT_FALSE(plane.empty());
}

TEST(planar, out_of_bounds) {
	SymbolChartSerializer serializer;
	std::stringstream stream("...\n"
							 "...\n"
							 "...");
	Plane plane;
	serializer.read(stream, plane);
	Cylinder cylinder(plane.getField());
	Torus torus(plane.getField());

//	EXPECT_ANY_THROW(plane.getSymbol({-1, 0}));
//	EXPECT_ANY_THROW(plane.getSymbol({0, -1}));
	EXPECT_ANY_THROW(plane.getSymbol({3, 0}));
	EXPECT_ANY_THROW(plane.getSymbol({0, 3}));

//	EXPECT_ANY_THROW(cylinder.getSymbol({-1, 0}));
//	EXPECT_ANY_THROW(cylinder.getSymbol({0, -1}));
	EXPECT_ANY_THROW(cylinder.getSymbol({3, 0}));
	EXPECT_ANY_THROW(cylinder.getSymbol({0, 3}));

//	EXPECT_ANY_THROW(torus.getSymbol({-1, 0}));
//	EXPECT_ANY_THROW(torus.getSymbol({0, -1}));
	EXPECT_ANY_THROW(torus.getSymbol({3, 0}));
	EXPECT_ANY_THROW(torus.getSymbol({0, 3}));
}

TEST(planar, distance_one_point) {
	SymbolChartSerializer serializer;
	std::stringstream stream(".");
	Plane plane;
	serializer.read(stream, plane);
	Cylinder cylinder(plane.getField());
	Torus torus(plane.getField());
	EXPECT_EQ(serializer.start, Planar::point_type());
	EXPECT_EQ(serializer.finish, Planar::point_type());
	EXPECT_EQ(plane.distance(serializer.start, serializer.finish), 0);
	EXPECT_EQ(cylinder.distance(serializer.start, serializer.finish), 0);
	EXPECT_EQ(torus.distance(serializer.start, serializer.finish), 0);
}

TEST(plane, distance) {
	SymbolChartSerializer serializer;
	std::stringstream stream("........F\n"
							 ".........\n"
							 "S........");
	Plane plane;
	serializer.read(stream, plane);
	EXPECT_EQ(plane.distance(serializer.start, serializer.finish), 10);
	EXPECT_EQ(plane.distance({0, 0}, {3, 2}), 5);
}

TEST(cylinder, distance) {
	SymbolChartSerializer serializer;
	std::stringstream stream("........F\n"
							 ".........\n"
							 "S........");
	Cylinder cylinder;
	serializer.read(stream, cylinder);
	EXPECT_EQ(cylinder.distance(serializer.start, serializer.finish), 3);
	EXPECT_EQ(cylinder.distance({0, 0}, {3, 2}), 5);
	EXPECT_EQ(cylinder.distance({7, 0}, {1, 2}), 5);
}

TEST(torus, distance) {
	SymbolChartSerializer serializer;
	std::stringstream stream("........F\n"
							 ".........\n"
							 "S........");
	Torus torus;
	serializer.read(stream, torus);
	EXPECT_EQ(torus.distance(serializer.start, serializer.finish), 2);
	EXPECT_EQ(torus.distance({0, 0}, {3, 2}), 4);
	EXPECT_EQ(torus.distance({7, 0}, {1, 2}), 4);
}

TEST(astar, empty) {
	Plane plane;
	PlanarRobot *star = nullptr;
	EXPECT_ANY_THROW(star = new PlanarRobot(plane));
	delete star;
}

TEST(astar, find_path_fail) {
	Plane plane;
	SymbolChartSerializer serializer;
	std::stringstream stream(".......#F\n"
							 "........#\n"
							 "S........");
	serializer.read(stream, plane);
	PlanarRobot star(plane);
	EXPECT_ANY_THROW(star.findPath(serializer.start, serializer.finish));
}

TEST(astar, find_path_plane) {
	Plane plane;
	SymbolChartSerializer serializer;
	std::stringstream stream("........F\n"
							 ".########\n"
							 "........S");
	serializer.read(stream, plane);
	PlanarRobot star(plane);
	star.findPath(serializer.start, serializer.finish);
}

TEST(astar, find_path_cylinder) {
	Cylinder cylinder;
	SymbolChartSerializer serializer;
	std::stringstream stream(".#......F\n"
							 ".########\n"
							 ".#......S");
	serializer.read(stream, cylinder);
	PlanarRobot star(cylinder);
	star.findPath(serializer.start, serializer.finish);
}

TEST(astar, find_path_torus) {
	Torus torus;
	SymbolChartSerializer serializer;
	std::stringstream stream("#F\n"
							 "##\n"
							 "#S");
	serializer.read(stream, torus);
	PlanarRobot star(torus);
	star.findPath(serializer.start, serializer.finish);
}

TEST(astar, find_path_words) {
	Dictionary dict;
	DictionarySerializer serializer;
	std::stringstream stream("dog\nfog\nlog\nlong\nfrog\nfoe\nlow\nbow\nbowl");
	serializer.read(stream, dict);
	LexicalRobot star(dict);
	auto path = star.findPath("dog", "bowl");
	EXPECT_EQ(path, (LexicalRobot::point_sequence{"dog", "log", "low", "bow", "bowl"}));
}

TEST(astar, find_path_print) {
	Plane plane;
	SymbolChartSerializer serializer;
	std::stringstream stream("........F\n"
							 ".########\n"
							 "........S");
	serializer.read(stream, plane);
	PlanarRobot star(plane);
	auto path = star.findPath(serializer.start, serializer.finish);
	std::stringstream pathStream("");
	serializer.printPath(pathStream, plane, path);
	std::string pathString = pathStream.str();
	EXPECT_EQ(pathString,
			  "********F\n"
			  "*########\n"
			  "********S\n");
}
