//
//  plane.cpp
//  explorer
//
//  Created by Иван Богуш on 11/22/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "plane.h"

using namespace std;

Plane::Plane():
Planar() {

}

Plane::Plane(const field_type& f):
Planar(f) {

}

Plane::metric_type Plane::horizontalDistance(const point_type &from, const point_type &to) const {
	return max(to.x, from.x) - min(to.x, from.x);
}

Plane::metric_type Plane::verticalDistance(const point_type &from, const point_type &to) const {
	return max(to.y, from.y) - min(to.y, from.y);
}

Plane::point_type Plane::westernNeighbor
(const point_value_type x, const point_value_type y) const {
	return Planar::westernNeighbor(x, y);
}

Plane::point_type Plane::easternNeighbor
(const point_value_type x, const point_value_type y) const {
	return Planar::easternNeighbor(x, y);
}

Plane::point_type Plane::northernNeighbor
(const point_value_type x, const point_value_type y) const {
	return Planar::northernNeighbor(x, y);
}

Plane::point_type Plane::southernNeighbor
(const point_value_type x, const point_value_type y) const {
	return Planar::southernNeighbor(x, y);
}
