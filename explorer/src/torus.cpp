//
//  torus.cpp
//  bogush
//
//  Created by Иван Богуш on 12/2/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "torus.h"

Torus::Torus(const field_type& field):
Planar(field) {

}

Torus::metric_type Torus::horizontalDistance(const point_type &from, const point_type &to) const {
	auto x1 = from.x;
	auto x2 = to.x;
	metric_type dx = std::max(x1, x2) - std::min(x1, x2);
	return std::min(dx, width - dx);
}

Torus::metric_type Torus::verticalDistance(const point_type& from, const point_type& to) const {
	auto y1 = from.y;
	auto y2 = to.y;
	metric_type dy = std::max(y1, y2) - std::min(y1, y2);
	return std::min(dy, height - dy);
}

Torus::point_type Torus::modulus(point_value_type x, point_value_type y) const {
	x %= width;
	y %= height;
	return point_type(x, y);
}

Torus::point_type Torus::modulus(const point_type& point) const {
	auto x = point.x;
	auto y = point.y;
	if (x < width && y < height) return point;
	return modulus(x, y);
}

Torus::point_type Torus::westernNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::westernNeighbor(x, y));
}

Torus::point_type Torus::easternNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::easternNeighbor(x, y));
}

Torus::point_type Torus::northernNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::northernNeighbor(x, y));
}

Torus::point_type Torus::southernNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::southernNeighbor(x, y));
}
