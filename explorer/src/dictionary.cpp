//
//  dictionary.cpp
//  bogush
//
//  Created by Иван Богуш on 12/9/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "dictionary.h"

#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

Dictionary::Dictionary(const field_type& field) {
	setData(field);
}

Dictionary&
Dictionary::setData(const field_type &field) {
	if (field.empty()) {
		throw runtime_error("Empty dictionary is prohibited");
	}

	for (auto word: field) {
		for (const auto& temp: templates(word)) {
			data[temp].insert(word);
		}
	}
	return *this;
}

Dictionary::data_type
Dictionary::getData() const {
	return data;
}

Dictionary::field_type
Dictionary::getField() const {
	field_type field;
	for (const auto& eachSet: data) {
		field.insert(field.end(), eachSet.second.begin(), eachSet.second.end());
	}
	return field;
}

Dictionary::metric_type
Dictionary::distance(const point_type& start, const point_type& end) const {
	if (start == end) return 0;
	if (start.length() == 0) return end.length();
	if (end.length() == 0) return start.length();
	// create two work vectors of integer distances
	vector<metric_type> prevRow(end.length() + 1);
	vector<metric_type> currentRow(end.length() + 1);

	// initialize v0 (the previous row of distances)
	// this row is A[0][i]: edit distance for an empty from
	// the distance is just the number of characters to delete from end
	for (metric_type i = 0; i < prevRow.size(); ++i) {
		prevRow[i] = i;
	}

	for (metric_type i = 0; i < start.size(); ++i)
	{
		// calculate v1 (current row distances) from the previous row v0

		// first element of v1 is A[i+1][0]
		//   edit distance is delete (i+1) chars from s to match empty t
		currentRow[0] = i + 1;

		// use formula to fill in the rest of the row
		for (metric_type j = 0; j < end.size(); ++j)
		{
			int cost = (start[i] == end[j] ? 0: 1);
			currentRow[j + 1] = min(min(currentRow[j] + 1, prevRow[j + 1] + 1), prevRow[j] + cost);
		}

		// copy v1 (current row) to v0 (previous row) for next iteration
		for (metric_type j = 0; j < prevRow.size(); ++j)
			prevRow[j] = currentRow[j];
	}

	return currentRow[end.size()];
}

Dictionary::point_set
Dictionary::lookup(const point_type& at) const {
	point_set result;
	size_t length = at.length();
	if (length == 0) {
		return result;
	}
	for (const auto& temp: templates(at)) {
		auto associatedPair = data.find(temp);
		if (associatedPair == data.end()) {
			continue;
		} else {
			auto associated = associatedPair->second;
			result.insert(associated.begin(), associated.end());
		}
	}
	return result;
}

bool
Dictionary::checkPath(const point_sequence& path, const point_type& from,
					  const point_type& to) const {
	if (from != path[0]) {
		return false;
	}
	auto prev = from;
	for (const auto& node: path) {
		if (node == prev) {
			continue;
		}
		auto neighbors = lookup(prev);
		if (find(neighbors.begin(), neighbors.end(), node) == neighbors.end()) {
			return false;
		}
		prev = node;
	}
	return prev == to;
}

bool
Dictionary::empty() const {
	return data.empty();
}

Dictionary::point_sequence
Dictionary::templates(const point_type &word) const {
	point_sequence result;
	// insert *
	for (size_t i = 0; i <= word.length(); ++i) {
		string wordTemplate = word;
		wordTemplate.insert(i, 1, '*');
		result.push_back(wordTemplate);
	}
	// replace with *
	for (size_t i = 0; i < word.length(); ++i) {
		string wordTemplate = word;
		wordTemplate[i] = '*';
		result.push_back(wordTemplate);
	}
	return result;
}
