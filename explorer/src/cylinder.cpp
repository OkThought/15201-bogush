//
//  cylinder.cpp
//  bogush
//
//  Created by Иван Богуш on 12/2/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "cylinder.h"

using namespace std;

Cylinder::Cylinder():
Planar() {

}

Cylinder::Cylinder(const field_type& f):
Planar(f) {

}

Cylinder::point_type Cylinder::modulus(point_value_type x, point_value_type y) const {
	x %= width;
	return point_type(x, y);
}

Cylinder::point_type Cylinder::modulus(const point_type& point) const {
	auto x = point.x;
	if (x < width) return point;
	return modulus(x, point.y);
}

Cylinder::metric_type Cylinder::horizontalDistance(const point_type &from, const point_type &to) const {
	auto x1 = from.x;
	auto x2 = to.x;
	metric_type dx = std::max(x1, x2) - std::min(x1, x2);
	return std::min(dx, width - dx);
}

Cylinder::metric_type Cylinder::verticalDistance(const point_type &from, const point_type &to) const {
	auto y1 = from.y;
	auto y2 = to.y;
	metric_type dy = std::max(y1, y2) - std::min(y1, y2);
	return dy;
}

Cylinder::point_type Cylinder::westernNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::westernNeighbor(x, y));
}

Cylinder::point_type Cylinder::easternNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::easternNeighbor(x, y));
}

Cylinder::point_type Cylinder::northernNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::northernNeighbor(x, y));
}

Cylinder::point_type Cylinder::southernNeighbor(point_value_type x, point_value_type y) const {
	return modulus(Planar::southernNeighbor(x, y));
}
