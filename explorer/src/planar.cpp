//
//  planar.cpp
//  bogush
//
//  Created by Иван Богуш on 12/7/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "planar.h"

#include <algorithm>

using namespace std;

Planar::Planar():
width(0), height(0), field() {

}

Planar::Planar(const field_type& f):
field(f) {
	if (f.empty() || f[0].empty()) {
		throw runtime_error("empty planar surface is prohibited");
	}
	width = f[0].size();
	height = f.size();
}

Planar::metric_type
Planar::distance(const point_type &from, const point_type &to) const {
	return horizontalDistance(from, to) + verticalDistance(from, to);
}

Planar::point_set
Planar::lookup(const point_type& at) const {
	point_set result;
	for (const auto neighbor: neighborsOf(at)) {
		if (free(neighbor)) {
			result.push_back(neighbor);
		}
	}
	return result;
}

bool
Planar::checkPath(const point_sequence& path, const point_type& from,
				  const point_type& to) const {
	if (from != path[0]) return false;
	auto prevNode = from;
	for (auto node: path) {
		if (!free(node)) {
			return false;
		}
		if (node != prevNode) {
			point_set neighbours = neighborsOf(prevNode);
			if (find(neighbours.begin(), neighbours.end(),
						  node) == neighbours.end()) {
				return false;
			}
		}
		prevNode = node;
	}
	return prevNode == to;
}

bool
Planar::empty() const {
	return field.empty();
}

Planar::symbol_type
Planar::getSymbol(const point_type& at) const {
	return getSymbol(at.x, at.y);
}

Planar::symbol_type
Planar::getSymbol(const point_value_type x, const point_value_type y) const {
	if (!exists(x, y)) throw runtime_error("out of bounds");
	return field[y][x] ? WALL : SPACE;
}

Planar::field_type
Planar::getField() const {
	return field;
}

Planar::metric_type
Planar::getHeight() const {
	return height;
}

Planar::metric_type
Planar::getWidth() const {
	return width;
}

Planar&
Planar::setSymbol(const point_type& p, const symbol_type s) {
	return setSymbol(p.x, p.y, s);
}

Planar&
Planar::setSymbol(const point_value_type x, const point_value_type y,
				  const symbol_type s) {
	if (!exists(x, y)) throw runtime_error("out of bounds");
	field[y][x] = s == WALL;
	return *this;
}

Planar&
Planar::setField(const Planar::field_type& f) {
	field = f;
	width = f[0].size();
	height = f.size();
	return *this;
}

bool
Planar::exists(const point_value_type x, const point_value_type y) const {
	if (y >= field.size() || x >= field[y].size()) return false;
	return true;
}

bool
Planar::exists(const point_type& there) const {
	return exists(there.x, there.y);
}

bool
Planar::free(const point_type& there) const {
	return free(there.x, there.y);
}

bool
Planar::free(const point_value_type x, const point_value_type y) const {
	return exists(x, y) && field[y][x] == SPACE;
}

Planar::point_type
Planar::westernNeighbor(point_value_type x, point_value_type y) const {
	return point_type(x-1, y);
}

Planar::point_type
Planar::easternNeighbor(point_value_type x, point_value_type y) const {
	return point_type(x+1, y);
}

Planar::point_type
Planar::northernNeighbor(point_value_type x, point_value_type y) const {
	return point_type(x, y-1);
}

Planar::point_type
Planar::southernNeighbor(point_value_type x, point_value_type y) const {
	return point_type(x, y+1);
}

Planar::point_type
Planar::westernNeighbor(const point_type &point) const {
	return westernNeighbor(point.x, point.y);
}

Planar::point_type
Planar::easternNeighbor(const point_type& point) const {
	return easternNeighbor(point.x, point.y);
}

Planar::point_type
Planar::northernNeighbor(const point_type& point) const {
	return northernNeighbor(point.x, point.y);
}

Planar::point_type
Planar::southernNeighbor(const point_type& point) const {
	return southernNeighbor(point.x, point.y);
}

Planar::point_set
Planar::neighborsOf(const point_type &point) const {
	return {
		westernNeighbor(point),
		northernNeighbor(point),
		easternNeighbor(point),
		southernNeighbor(point)
	};
}
