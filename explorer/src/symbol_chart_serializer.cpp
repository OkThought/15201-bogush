//
//  symbol_chart_serializer.cpp
//  bogush
//
//  Created by Иван Богуш on 12/7/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "symbol_chart_serializer.h"

#include <sstream>

using namespace std;

void SymbolChartSerializer::read(istream &is, Planar &planar) {
	vector<string> data;
	string line;
	size_t width;
	if (getline(is, line)) {
		width = line.length();
		if (width == 0) {
			throw runtime_error("found empty line\n");
		}
	} else {
		throw runtime_error("empty surface is prohibited");
	}
	data.push_back(line);
	while (getline(is, line)) {
		if (line.length() != width) {
			throw runtime_error("planar surface should be rectangular");
		}
		data.push_back(line);
	}
	Planar::field_type field;
	point_type point;
	bool startSet = false;
	bool finishSet = false;
	for (auto line: data) {
		Planar::field_type::value_type row;
		for (auto each: line) {
			row.push_back(each == Wall);
			if (each == Start) {
				if (startSet) {
					throw runtime_error("multiple starting points found");
				}
				startSet = true;
				start = point_type(point);
			} else if (each == Finish) {
				if (finishSet) {
					throw runtime_error("multiple finishing points found");
				}
				finishSet = true;
				finish = point_type(point);
			} else if (each != Space && each != Wall) {
				string character;
				stringstream stream;
				stream << each;
				stream >> character;
				throw runtime_error(character + " is a bad symbol");
			}
			++point.x;
		}
		field.push_back(row);
		point.x = 0;
		++point.y;
	}
	planar.setField(field);
}

void SymbolChartSerializer::print(ostream &os, const Planar &planar) {
	auto field = planar.getField();
	for (auto row: field) {
		string line;
		for (auto val: row) {
			line.push_back(val ? Wall : Space);
		}
		os << line << endl;
	}
}

void SymbolChartSerializer::printPath(ostream &os, const Planar &planar, const Planar::point_sequence &path) {
	auto field = planar.getField();
	vector<string> lines;
	for (auto row: field) {
		string line;
		for (auto val: row) {
			line.push_back(val ? Wall : Space);
		}
		lines.push_back(line);
	}

	for (auto node: path) {
		lines[node.y][node.x] = Path;
	}

	lines[start.y][start.x] = Start;
	lines[finish.y][finish.x] = Finish;

	for (auto line: lines) {
		os << line << endl;
	}
}
