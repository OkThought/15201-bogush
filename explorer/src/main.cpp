//
//  main.cpp
//  oop
//
//  Created by Иван Богуш on 11/28/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "plane.h"
#include "cylinder.h"
#include "torus.h"
#include "dictionary.h"
#include "astar_robot.h"
#include "anyoption.h"
#include "factory.h"
#include "symbol_chart_serializer.h"
#include "dictionary_serializer.h"

#include <iostream>
#include <fstream>
#include <unordered_map>

using namespace std;

void addUsage(AnyOption &opt);
void setOptionsAndFlags(AnyOption &opt);
vector<string> split(const string& str, const string& delimiter);

struct HammingDistance {
	size_t operator() (const string& left, const string& right) {
		auto leftLength = left.length();
		auto rightLenght = right.length();
		if (leftLength != rightLenght) return min(leftLength, rightLenght);
		size_t dist = 0;
		size_t i = 0;
		for (auto sym: left) {
			if (sym != right[i++]) {
				dist++;
			}
		}
		return dist++;
	};
};

int main(int argc, char **argv) {
	AnyOption *opt = new AnyOption();
	addUsage(*opt);
	setOptionsAndFlags(*opt);

	opt->processCommandArgs(argc, argv);

	if(!opt->hasOptions()) { /* print usage if no options */
		opt->printUsage();
		delete opt;
		return 0;
	}

	size_t optionCounter = 0;
	size_t flagCounter = 0;

	if (opt->getFlag("help") || opt->getFlag('h')) {
		flagCounter++;
		opt->printUsage();
		delete opt;
		return 0;
	}

	string inputFile = "space.txt";
	string outputFile = "";
	size_t limit = 1000;

	if (opt->getValue("input") || opt->getValue('i')) {
		optionCounter++;
		inputFile = opt->getValue('i');
	}

	if (opt->getValue("out") || opt->getValue('o')) {
		optionCounter++;
		outputFile = opt->getValue("out");
	}

	if (opt->getValue("limit") || opt->getValue('l')) {
		optionCounter++;
		limit = atoi(opt->getValue("limit"));
	}

	string topology = "plane";
	if (opt->getValue("topology") || opt->getValue('t')) {
		optionCounter++;
		topology = opt->getValue("topology");
	}

	string start;
	string finish;
	if (opt->getValue("start") || opt->getValue("finish")) {
		optionCounter++;
		start = opt->getValue("start");
		finish = opt->getValue("finish");
		if (topology != "words") {
			cout << "start/finish point can be specified only for 'words' topology\n";
			delete opt;
			return 0;
		}
	}

	if (flagCounter + optionCounter == 0) {
		if (argc > 1) {
			cout << "wrong flag/option '" << argv[1] << "'\n";
		}
		opt->printUsage();
		delete opt;
		return 0;
	}

	if (inputFile.length() == 0) {
		cout << "no input file specified\n";
		delete opt;
		return 0;
	}

	ifstream fin(inputFile);
	try {
		if (topology == "words") {
			DictionarySerializer serializer;
			unique_ptr<Dictionary> dictp(new Dictionary);
			auto& dict = *dictp;
			const auto& surface = reinterpret_cast<const Dictionary::Surface&>(dict);
			serializer.read(fin, dict);
			AStarRobot<Dictionary::Surface, HammingDistance> star(surface);
			auto path = star.findPath(start, finish);
			serializer.printPath(cout, dict, path);
		} else {
			PlanarFactory factory;
			factory.add<Plane>("plane").add<Cylinder>("cylinder").add<Torus>("torus");
			auto surface = factory.create(topology);
			if (!surface) {
				std::cout << "wrong parameter for topology type: " << topology<< std::endl;
				opt->printUsage();
				delete opt;
				return 0;
			}
			SymbolChartSerializer serializer;
			serializer.read(fin, *surface);
			AStarRobot<Planar::Surface> star(*surface);
			auto path = star.findPath(serializer.start, serializer.finish);
			if (outputFile.length()) {
				ofstream fout(outputFile);
				serializer.printPath(fout, *surface, path);
			} else {
				serializer.printPath(cout, *surface, path);
			}
		}
	} catch (exception& e) {
		cout << e.what() << endl;
	}
	delete opt;
	return 0;
}

void addUsage(AnyOption &opt) {
	opt.addUsage("														");
	opt.addUsage("Usage:												");
	opt.addUsage("														");
	opt.addUsage("	-h --help											");
	opt.addUsage("		Prints this help								");
	opt.addUsage("														");
	opt.addUsage("	-i --input <file>									");
	opt.addUsage("		Specify a text file with space description		");
	opt.addUsage("		`file` is \"space.txt\" by default				");
	opt.addUsage("														");
	opt.addUsage("	-o --out <file>										");
	opt.addUsage("		Specify a text file to print the path to		");
	opt.addUsage("		prints to stdout by default						");
	opt.addUsage("														");
	opt.addUsage("	-l --limit <number>									");
	opt.addUsage("		Specify a max search distance					");
	opt.addUsage("		`number` is 1000 by default						");
	opt.addUsage("														");
	opt.addUsage("	-t --topology <type>								");
	opt.addUsage("		Specify a type of topology						");
	opt.addUsage("														");
	opt.addUsage("		possible types:									");
	opt.addUsage("			plane - simple rectangular planar surface	");
	opt.addUsage("			cylinder - plane with right and left bounds	");
	opt.addUsage("				connected								");
	opt.addUsage("			torus - cylinder with upper and down bounds ");
	opt.addUsage("				connected								");
	opt.addUsage("			words - list of words with Levenshtein's	");
	opt.addUsage("				distance algorithm						");
	opt.addUsage("														");
	opt.addUsage("		`type` is \"plane\" by default					");
	opt.addUsage("														");
	opt.addUsage("	--start <point>										");
	opt.addUsage("		Specify a point to start the search from		");
	opt.addUsage("														");
	opt.addUsage("	--finish <point>									");
	opt.addUsage("		Specify a point to finish the search at			");
	opt.addUsage("														");
}

void setOptionsAndFlags(AnyOption &opt) {
	opt.setFlag("help", 'h');
	opt.setOption("input", 'i');
	opt.setOption("out", 'o');
	opt.setOption("limit", 'l');
	opt.setOption("topology", 't');
	opt.setOption("start");
	opt.setOption("finish");
}
