//
//  dictionary_serializer.cpp
//  bogush
//
//  Created by Иван Богуш on 12/12/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include "dictionary_serializer.h"

#include <string>

using namespace std;

void DictionarySerializer::read(std::istream& is, Dictionary& dict) {
	Dictionary::field_type field;
	string word;
	while (getline(is, word)) {
		field.push_back(word);
	}
	dict.setData(field);
}

void DictionarySerializer::read(std::istream& is, Dictionary& dict, const word_type& delimiter) {
	Dictionary::field_type field;
	string line;
	while (getline(is, line)) {
		auto words = split(line, delimiter);
		field.insert(field.end(), words.begin(), words.end());
	}
	dict.setData(field);
}

void DictionarySerializer::print(ostream& os, const Dictionary& dict) {
	for (const auto& word: dict.getField()) {
		os << word << endl;
	}
}

void DictionarySerializer::printPath(ostream& os, const Dictionary&, const point_sequence& path) {
	for (const auto& word: path) {
		os << word << endl;
	}
}

DictionarySerializer::word_set DictionarySerializer::split(const word_type& str, const word_type& delimiter) {
	word_set result;
	word_type::size_type pos;
	word_type copy = str;
	while ((pos = copy.find(delimiter)) != word_type::npos) {
		auto word = copy.substr(0, pos);
		result.push_back(word);
		copy.replace(0, pos + delimiter.length(), "");
	}
	result.push_back(copy);
	return result;
}
