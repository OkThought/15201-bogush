//
//  trit.cpp
//  bogush
//
//  Created by Иван Богуш on 9/23/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#include <iostream>
#include <unordered_map>
#include <cstring> // for memcpy

#include "tritset.h"

// static methods and functions

uint setBit(uint &src, const size_t pos, uint bit) {
	if (bit) {
		src |=  (1 << pos);
	} else {
		src &= ~(1 << pos);
	}
	return src;
}

uint setTrit(uint &src, size_t pos, Trit trit) {
	setBit(src, pos * 2 + 1, trit & 2u); // i0
	setBit(src, pos * 2 + 0, trit & 1u); // 0j
	return src;
}

size_t invertedLocalUintPosition(size_t pos) {
	size_t uintPos = pos / tritsInUint;
	size_t localPos = pos - uintPos * tritsInUint;
	size_t invertedLocalPos = tritsInUint - 1 - localPos;
	return invertedLocalPos;
}

std::string tritToString(const Trit t) {
	switch (t) {
		case T:
			return "T";
		case F:
			return "F";
		case U:
			return "U";
		default:
			return "NS";
	}
}

Trit operator~ (const Trit t) {
	if (t == F) {
		return T;
	} else if (t == T) {
		return F;
	} else {
		return U;
	}
}

Trit operator& (const Trit a, const Trit b) {
	if (a == F || b == F) {
		return F;
	} else if (a == U || b == U) {
		return U;
	} else {
		return T;
	}
}

Trit operator| (const Trit a, const Trit b) {
	if (a == T || b == T) {
		return T;
	} else if (a == U || b == U) {
		return U;
	} else {
		return F;
	}
}

TritSet operator& (const TritSet& left, const TritSet& right) {
	TritSet result = left;
	result &= right;
	return result;
}

TritSet operator| (const TritSet& left, const TritSet& right) {
	TritSet result = left;
	result |= right;
	return result;
}

std::ostream& operator<< (std::ostream& os, const TritSet ts) {
	for (const auto& i : ts) {
		os << i.toString();
	}
	return os;
}

// TritSet
// TritSet::constructors

TritSet::TritSet(): size(0), data(nullptr) { }

TritSet::TritSet(const size_t length) {
	size = 1 + (length - 1) / tritsInUint; // ceil
	data = new uint[size]();
}

TritSet::TritSet(const TritSet &trs) {
	data = new uint[size = trs.size];
	memcpy(data, trs.data, size * sizeof(uint));
}

TritSet::TritSet(TritSet &&trs): size(trs.size), data(trs.data) {
	trs.data = nullptr;
}

TritSet::TritSet(std::initializer_list<Trit> il) {
	size_t len = il.size();
	if (len > 0) {
		size = 1 + (len - 1) / tritsInUint; // ceil
		data = new uint[size]();
		for (size_t i = 0; i < len; i++) {
			(*this).setTrit(i, il.begin()[i]);
		}
	}
}

// TritSet::destructor

TritSet::~TritSet() {
	delete[] data;
	data = nullptr;
}

// TritSet::public methods

inline size_t TritSet::capacity() const {
	return size * tritsInUint;
}

inline size_t TritSet::length() const {
	if (0 == size) return 0;
	size_t uintPos = size;
	// skip 0 uints
	while (0 == data[uintPos - 1]) {
		if (uintPos == 0) {
			return 0;
		}
		--uintPos;
	}
	uint currentUint = data[uintPos - 1];
	size_t invertedLocalPos = 0;
	// skip Unknown trits in current_uint
	while (!(currentUint & tritBitMask)) {
		currentUint >>= 2;
		++invertedLocalPos;
	}
	return uintPos * tritsInUint - invertedLocalPos;
}

TritSet& TritSet::shrink() {
	if (0 == size) return *this;
	size_t oldSize = size;
	while (0 == data[size-1]) {
		if (0 == --size) {
			delete[] data;
			data = nullptr;
			return *this;
		}
	}
	if (size < oldSize) {
		resize(size);
	}
	return *this;
}

TritSet& TritSet::trim(size_t toPosition) {
	auto cap = capacity();
	if (toPosition >= cap) return *this;
	auto newSize = 1 + (toPosition - 1) / tritsInUint; // ceil
	if (newSize < size) {
		resize(newSize);
	}
	for (size_t i = cap-1; i >= toPosition; --i) {
		(*this)[i] = U;
	}
	return *this;
}

size_t TritSet::cardinality(Trit trit) const {
	size_t card = 0;
	for (const auto& t : *this) {
		if ((const Trit)t == trit) {
			++card;
		}
	}
	return card;
}

TritSet::map TritSet::cardinality() const {
	TritSet::map m;
	for (const auto& t : *this) {
		++m[t];
	}
	return m;
}

TritSet::iterator TritSet::begin() {
	return iterator(*this, 0, length());
}

TritSet::iterator TritSet::end() {
	auto len = length();
	return iterator(*this, len, len);
}

TritSet::const_iterator TritSet::begin() const {
	return const_iterator(*this, 0, length());
}

TritSet::const_iterator TritSet::end() const {
	auto len = length();
	return const_iterator(*this, len, len);
}

// TritSet::operators

TritSet& TritSet::operator= (const TritSet &trs) {
	if (this != &trs) {
		delete[] data;
		data = new uint[size = trs.size];
		memcpy(data, trs.data, size * sizeof(uint));
	}
	return *this;
}

TritSet& TritSet::operator= (TritSet &&trs) {
	size = trs.size;
	data = trs.data;
	trs.size = 0;
	trs.data = nullptr;
	return *this;
}

TritSet& TritSet::operator&= (const TritSet &ts) {
	for (size_t i = std::max(ts.length(), length()); i > 0; i--) {
		(*this)[i-1] = (*this)[i-1] & ts[i-1];
	}
	return *this;
}

TritSet& TritSet::operator|= (const TritSet &ts) {
	for (size_t i = std::max(ts.length(), length()); i > 0; i--) {
		(*this)[i-1] = (*this)[i-1] | ts[i-1];
	}
	return *this;
}

TritSet TritSet::operator~ () {
	size_t cap = capacity();
	TritSet result(cap);
	for (size_t i = 0; i < cap; ++i) {
		result[i] = ~(*this)[i];
	}
	return result;
}

TritSet::const_trit_holder TritSet::operator[] (size_t pos) const {
	return const_trit_holder(*this, pos);
}

TritSet::trit_holder TritSet::operator[] (size_t pos) {
	return trit_holder(*this, pos);
}

// TritSet::private methods

void TritSet::extendCapacity(size_t toPos) {
	if (toPos < capacity()) return;
	size_t newCapacity = toPos + 1;
	size_t newSize = 1 + (newCapacity - 1) / tritsInUint; // ceil
	resize(newSize);
}

void TritSet::setTrit(size_t pos, Trit trit) {
	if (pos >= capacity()) {
		if (trit == U) return;
		extendCapacity(pos);
	}
	size_t uintPos = pos / tritsInUint;
	size_t invertedLocalPos = invertedLocalUintPosition(pos);
	::setTrit(data[uintPos], invertedLocalPos, trit);
}

void TritSet::resize(size_t toSize) {
	if (toSize == 0) {
		delete data;
		data = nullptr;
		size = 0;
		return;
	}
	uint *newData = new uint[toSize];
	if (size > 0) {
		memcpy(newData, data, toSize * sizeof(uint));
	}
	delete[] data;
	data = newData;
	size = toSize;
}

Trit TritSet::getTritAt(const size_t pos) const {
	size_t uintPos = pos / tritsInUint;
	size_t shift = invertedLocalUintPosition(pos) * bitsInTrit;
	return (Trit) ((data[uintPos] >> shift) & tritBitMask);
}

// TritSet::TritHash

uint TritSet::TritHash::operator() (Trit trit) const {
	return (uint) trit;
}
