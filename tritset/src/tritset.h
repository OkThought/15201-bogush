//
//  trit.h
//  bogush
//
//  Created by Иван Богуш on 9/23/16.
//  Copyright © 2016 Ivan Bogush. All rights reserved.
//

#ifndef tritset_h
#define tritset_h

#include <iostream> 
#include <climits> // for CHAR_BIT
#include <unordered_map>

enum Trit { U = 0, F = 1, T = 2 };
const uint bitsInTrit = 2;
const uint tritBitMask = 3;
const uint bitsInUint = sizeof(uint) * CHAR_BIT;
const uint tritsInUint = bitsInUint / bitsInTrit;

uint setBit(uint &where, size_t position, uint bit);
uint setTrit(uint &where, size_t position, Trit trit);
size_t invertedLocalUintPosition(size_t pos);
std::string tritToString(const Trit);

class TritSet {
private:
	template<typename SetType> class TritHolder;
	template <typename SetType> class Iterator;
public:
	using const_trit_holder = TritHolder<const TritSet>;
	using trit_holder = TritHolder<TritSet>;

	using iterator = Iterator<TritSet>;
	using const_iterator = Iterator<const TritSet>;

	class TritHash {
	public:
		uint operator() (Trit trit) const;
	};
	using map = std::unordered_map<Trit, uint, TritHash>;

	TritSet();
	TritSet(const size_t length);
	TritSet(const TritSet&);
	TritSet(TritSet&&);
	TritSet(std::initializer_list<Trit>);
	~TritSet();

	size_t capacity() const;
	size_t length() const;
	size_t cardinality(Trit trit) const;
	map cardinality() const;
	const_iterator begin() const;
	const_iterator end() const;
	operator std::string() const;
	const_trit_holder operator[] (size_t) const;


	iterator begin();
	iterator end();
	TritSet& shrink();
	TritSet& trim(size_t toPosition);
	TritSet& operator= (const TritSet&);
	TritSet& operator= (TritSet&&);
	TritSet& operator&= (const TritSet&);
	TritSet& operator|= (const TritSet&);
	TritSet operator~ ();
	trit_holder operator[] (size_t);
private:
	void extendCapacity(size_t toPosition);
	void setTrit(size_t atPosition, Trit trit);
	void resize(size_t toSize);
	Trit getTritAt(const size_t pos) const;

	size_t size;
	uint *data;

	template<typename SetType>
	class TritHolder {
	public:
		TritHolder(SetType &set, size_t position);
		TritHolder& operator= (const TritHolder&);
		TritHolder& operator= (const Trit&);
		bool operator== (const Trit);
		operator Trit() const;
		std::string toString() const;
	private:
		SetType &set;
		size_t pos;
		Trit value;
	};

	template <typename SetType>
	class Iterator {
	public:
		Iterator(SetType&, size_t pos, size_t len);
		Iterator(const Iterator&); // copy constructor

		bool operator== (Iterator const&) const;
		bool operator!= (Iterator const&) const;
		TritHolder<SetType>* operator->();
		TritHolder<SetType> operator*();
		Iterator& operator++();
		Iterator operator++(int);
	private:
		SetType &set;
		size_t pos;
		size_t len;
		TritHolder<SetType> tritHolder;
	};
};

Trit operator~ (const Trit);
Trit operator& (const Trit, const Trit);
Trit operator| (const Trit, const Trit);

TritSet operator& (const TritSet&, const TritSet&);
TritSet operator| (const TritSet&, const TritSet&);

std::ostream& operator<< (std::ostream&, const TritSet);


// TritSet::TritHolder

template<typename SetType>
TritSet::TritHolder<SetType>::TritHolder(SetType &s, size_t p)
: set(s), pos(p) {
	if (pos < set.capacity()) {
		value = set.getTritAt(pos);
	} else {
		value = U;
	}
}

template<typename SetType>
TritSet::TritHolder<SetType>& TritSet::TritHolder<SetType>::operator= (const TritHolder& other) {
	if (&set != &other.set) {
		throw std::invalid_argument("cannot assign to TritHolder with different\
									set reference");
	}
	pos = other.pos;
	value = other.value;
	return *this;
}

template<typename SetType>
TritSet::TritHolder<SetType>& TritSet::TritHolder<SetType>::operator=
(const Trit& trit) {
	set.setTrit(pos, trit);
	value = trit;
	return *this;
}

template<>
TritSet::const_trit_holder& TritSet::const_trit_holder::operator= (const Trit&) = delete;

template<typename SetType>
std::string TritSet::TritHolder<SetType>::toString() const {
	return tritToString(value);
}

template<typename SetType>
bool TritSet::TritHolder<SetType>::operator== (const Trit trit) {
	return value == trit;
}

template<typename SetType>
TritSet::TritHolder<SetType>::operator Trit() const {
	return value;
}

// TritSet::Iterator

template<typename SetType>
TritSet::Iterator<SetType>::Iterator(SetType &s, size_t p, size_t l):
	set(s),
	pos(p),
	len(l),
	tritHolder(TritHolder<SetType>(set, pos))
{
}

template<typename SetType>
TritSet::Iterator<SetType>::Iterator(const Iterator &iter):
	set(iter.set),
	pos(iter.pos),
	len(iter.len),
	tritHolder(iter.tritHolder)
{
}

template<typename SetType>
TritSet::Iterator<SetType>& TritSet::Iterator<SetType>::operator++() {
	if (++pos > len) {
		throw std::out_of_range("Out of range while incrementing the iterator");
	}
	return *this;
}

template<typename SetType>
TritSet::Iterator<SetType> TritSet::Iterator<SetType>::operator++(int) {
	Iterator old = *this;
	++*this;
	return old;
}

template<typename SetType>
bool TritSet::Iterator<SetType>::operator== (const Iterator& iter) const {
	if (&set != &iter.set) {
		throw std::invalid_argument("iterators of different sets are incomparable");
	}
	if (pos >= len && iter.pos >= len) {
		// left and right are invalid
		// then they are equal
		return true;
	} else if (pos >= len || iter.pos >= len) {
		// one of them is invalid
		// then they are not equal
		return false;
	} else {
		return pos == iter.pos;
	}
}

template<typename SetType>
bool TritSet::Iterator<SetType>::operator!= (const Iterator& iter) const {
	return !(*this == iter);
}

template<typename SetType>
TritSet::TritHolder<SetType> TritSet::Iterator<SetType>::operator* () {
	tritHolder = set[pos];
	return tritHolder;
}

template<typename SetType>
TritSet::TritHolder<SetType>* TritSet::Iterator<SetType>::operator-> () {
	tritHolder = set[pos];
	return &tritHolder;
}



#endif /* tritset_h */
