#include "tritset.h"
#include "gtest/gtest.h"

TEST(tritset_public_methods, copy_operator) {
	TritSet set = {U,T,F};
	TritSet other = set;
	for (size_t i = 0; i < set.length(); ++i) {
		EXPECT_EQ(set[i], other[i]);
	}
	other[0] = T;
	EXPECT_NE(set[0], other[0]);
}

TEST(tritset_public_methods, move_operator) {
	TritSet a = {U,T,F};
	TritSet b;
	auto cap = a.capacity();
	b = std::move(a);
	EXPECT_EQ(a.capacity(), 0);
	EXPECT_EQ(b.capacity(), cap);
}

TEST(tritset_public_methods, square_brackets_operator_get) {
	TritSet set = {U,T,F};
	EXPECT_EQ(set[0], U);
	EXPECT_EQ(set[1], T);
	EXPECT_EQ(set[2], F);
}

TEST(tritset_public_methods, square_brackets_operator_get_out_of_bounds) {
	TritSet set = {U,T,F};
	auto cap = set.capacity();
	EXPECT_EQ(set[10000], U);
	EXPECT_EQ(set.capacity(), cap);
}

TEST(tritset_public_methods, square_brackets_operator_set) {
	TritSet set;
	set[0] = T;
	EXPECT_EQ(set[0], T);
	EXPECT_GT(set.capacity(), 0);
}

TEST(tritset_public_methods, square_brackets_operator_set_unknown) {
	TritSet set = {U,T,F};
	auto cap = set.capacity();
	set[1000] = U;
	EXPECT_EQ(set.capacity(), cap);
}

TEST(tritset_public_methods, and_operator) {
	TritSet a =		 {U,U,U,T,T,T,F,F,F};
	TritSet b =		 {U,T,F,U,T,F,U,T,F};
	TritSet expect = {U,U,F,U,T,F,F,F,F};
	TritSet result = a & b;
	for (size_t i = 0; i < result.length(); ++i) {
		EXPECT_EQ(result[i], expect[i]);
	}
}

TEST(tritset_public_methods, or_operator) {
	TritSet a =		 {U,U,U,T,T,T,F,F,F};
	TritSet b =		 {U,T,F,U,T,F,U,T,F};
	TritSet expect = {U,T,U,T,T,T,U,T,F};
	TritSet result = a | b;
	for (size_t i = 0; i < result.length(); ++i) {
		EXPECT_EQ(result[i], expect[i]);
	}
}

TEST(tritset_public_methods, not_operator) {
	TritSet a =		 {U,T,F};
	TritSet expect = {U,F,T};
	TritSet result = ~a;
	for (size_t i = 0; i < result.length(); ++i) {
		EXPECT_EQ(result[i], expect[i]);
	}
}

TEST(tritset_public_methods, capacity_empty) {
	TritSet set;
	EXPECT_EQ(set.capacity(), 0);
}

TEST(tritset_public_methods, capacity_non_empty) {
	const size_t tritNumber = 10;
	TritSet set(tritNumber);
	const size_t expectedSize = 1 + (tritNumber - 1) / tritsInUint;
	const size_t expectedCapacity = expectedSize * tritsInUint;
	EXPECT_EQ(set.capacity(), expectedCapacity);
}

TEST(tritset_public_methods, length_empty) {
	TritSet set;
	EXPECT_EQ(set.length(), 0);
}

TEST(tritset_public_methods, length_non_empty) {
	TritSet set(1);
	EXPECT_EQ(set.length(), 0);
	set[0] = F;
	EXPECT_EQ(set.length(), 1);
	set[1] = T;
	EXPECT_EQ(set.length(), 2);
	set[2] = U;
	EXPECT_EQ(set.length(), 2);
}

TEST(tritset_public_methods, shrink_empty) {
	TritSet a;
	a.shrink();
	EXPECT_EQ(a.capacity(), 0);
	TritSet b(32);
	b.shrink();
	EXPECT_EQ(b.capacity(), 0);
}

TEST(tritset_public_methods, shrink_non_empty) {
	TritSet a = {U,T,F};
	auto oldCapacity = a.capacity();
	a.shrink();
	EXPECT_EQ(a.capacity(), oldCapacity);
}

TEST(tritset_public_methods, trim_empty) {
	TritSet a;
	a.trim(0);
	EXPECT_EQ(a.capacity(), 0);
	EXPECT_EQ(a.length(), 0);
	EXPECT_EQ(a[0], U);
	TritSet b(32);
	b.trim(1);
	EXPECT_EQ(a.capacity(), 0);
	EXPECT_EQ(a.length(), 0);
	EXPECT_EQ(a[0], U);
}

TEST(tritset_public_methods, trim_non_empty) {
	TritSet a = {U,T,F};
	auto oldCapacity = a.capacity();
	a.trim(3);
	EXPECT_EQ(a.capacity(), oldCapacity);
	EXPECT_EQ(a.length(), 3);
	EXPECT_EQ(a[2], F);
	a.trim(2);
	EXPECT_EQ(a.capacity(), oldCapacity);
	EXPECT_EQ(a.length(), 2);
	EXPECT_EQ(a[2], U);
}

TEST(tritset_public_methods, trit_cardinality_empty) {
	TritSet set;
	EXPECT_EQ(0, set.cardinality(U));
	EXPECT_EQ(0, set.cardinality(T));
	EXPECT_EQ(0, set.cardinality(F));
}

TEST(tritset_public_methods, trit_cardinality_non_empty) {
	TritSet set = {U,T,F,U,T,T,F,F,F};
	EXPECT_EQ(2, set.cardinality(U));
	EXPECT_EQ(3, set.cardinality(T));
	EXPECT_EQ(4, set.cardinality(F));
}

TEST(tritset_public_methods, cardinality_empty) {
	TritSet set = {};
	auto map = set.cardinality();
	EXPECT_EQ(map[U], 0);
	EXPECT_EQ(map[T], 0);
	EXPECT_EQ(map[F], 0);
}

TEST(tritset_public_methods, cardinality_non_empty) {
	TritSet set = {U,T,T,F,F,F};
	auto map = set.cardinality();
	EXPECT_EQ(map[U], 1);
	EXPECT_EQ(map[T], 2);
	EXPECT_EQ(map[F], 3);
}

TEST(tritset_public_methods, const_iterator) {
	const TritSet set = {U,T,F,U,T,F};
	for (const auto &trit : set) {
		std::cout << trit.toString();
	}
	std::cout << std::endl;
}

TEST(tritset_public_methods, iterator) {
	TritSet set = {U,T,F,U,T,F};
	auto len = set.length();
	for (auto tritHolder : set) {
		tritHolder = U;
	}
	for (size_t i = 0; i < len; ++i) {
		EXPECT_EQ(set[i], U);
	}
}

